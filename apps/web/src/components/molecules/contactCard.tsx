import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import React from "react";
import {InlineEdit} from "@components/molecules/inlineEdit";

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  const [isEditing, setIsEditing] = React.useState(false);
  const [newName, setNewName] = React.useState('')
  const [error, setError] = React.useState(false)
  console.log(newName)

  const handleEdit = () => {
    setIsEditing(true)
  }
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
            {isEditing ? <InlineEdit setIsEditing={setIsEditing} setNewName={setNewName} setError={setError} /> :
            <Typography onClick={handleEdit} variant="subtitle1" lineHeight="1rem">
              {name}
            </Typography>
            }
          <Typography variant="caption" color="text.secondary">
            {email}
          </Typography>
          {error && <p>Validation ERROR</p>}
        </Box>
      </Box>
    </Card>
  );
};
