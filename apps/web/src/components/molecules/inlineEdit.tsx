import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import React from 'react';

export interface IInlineEditProps {
  setIsEditing: Function;
  setNewName: Function;
  setError: Function;
}

export const InlineEdit: React.FC<IInlineEditProps> = ({ setIsEditing, setNewName, setError }) => {
  const [newValue, setNewValue] = React.useState('')
  const handleSubmit = () => {
    setIsEditing(false)
    if (newValue.length > 0) {
      setNewName(newValue)
      setError(false)
    } else (
      setError(true)
    )
  }

  const handleChange = e => {
    setNewValue(e.target.value)
  }
  return (
    <React.Fragment>
      <input value={newValue} onChange={handleChange} type="text"/>
      <button onClick={handleSubmit}>Ok</button>
      <button onClick={() => setIsEditing(false)}>Cancel</button>
    </React.Fragment>
  );
};
